# E_is_For_egg

Adding to documentation

A Public project experimenting with animating SVG image.

Graphic design courtesy of Yevgeniya Tanasiychuk <i.am.jennietan@gmail.com>

Possible animation libraries or examples:

http://animejs.com/
https://hn.algolia.com/?query=javascript%20animation&sort=byPopularity&prefix&page=0&dateRange=all&type=story

Eyes animation
https://codepen.io/J-Roel/pen/wWGNQN

Animated Alien
https://scotch.io/bar-talk/build-an-eye-tracking-alien-with-javascript-solution-to-code-challenge-4
